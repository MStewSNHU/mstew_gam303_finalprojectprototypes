*Began hover control implementation
>The tutorial offered does not seem to work with the current version of UE. I was able to activate physics, 
>but despite adding force, the object failed to hover and would simply fall to the ground.

*Implemented sound, including spatial audio and falloff
>The sound is only heard near the connected object and fades as you walk away.

*Created a flat landscape

*Created a simple static skybox